.PHONY: clean test release

default_target: release

MakeJobs = 8
ifdef MAKE_JOBS
	MakeJobs = $(MAKE_JOBS)
endif

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:

clean:
	rm -rf ./build

release: clean
	mkdir build
	cd build && cmake .. && make -j $(MakeJobs)

test: clean
	mkdir build
	cd build && cmake .. -DIS_TEST=ON && make -j $(MakeJobs) && ctest -j $(MakeJobs) --rerun-failed --output-on-failure
