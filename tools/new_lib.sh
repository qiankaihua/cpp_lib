#!/bin/bash

curPath=$(pwd)
cd $(dirname $0)

function cdAndExit() {
    cd $curPath
    exit $1
}

if [ $# -ne 1 ]; then
    echo "Usage: $0 <new lib name>"
    cdAndExit -1
fi

new_lib_name=$1
capitalized_new_lib_name=$(echo "$new_lib_name" | awk '{print toupper(substr($0,1,1)) substr($0,2)}')

if [ -d ../src/$new_lib_name ]; then
    echo "Error: lib name <$new_lib_name> is alreadly exist."
    cdAndExit -1
fi

new_lib_dir=../src/$new_lib_name
mkdir -p $new_lib_dir

echo "#pragma once

namespace q_cpp_lib {
namespace $new_lib_name {

} // namespace $new_lib_name
} // namespace q_cpp_lib" > $new_lib_dir/$new_lib_name.h
echo "#include \"$new_lib_name.h\"

namespace q_cpp_lib {
namespace $new_lib_name {

} // namespace $new_lib_name
} // namespace q_cpp_lib" > $new_lib_dir/$new_lib_name.cc
echo "#include \"$new_lib_name.h\"

#include <gtest/gtest.h>

#include \"test_common/test_common.h\"

namespace q_cpp_lib {
namespace $new_lib_name {

TEST(${capitalized_new_lib_name}Test, BasicTest) {
    {
        SubTestLog(\"${new_lib_name} init\");
    }
}

} // namespace $new_lib_name
} // namespace q_cpp_lib" > $new_lib_dir/${new_lib_name}_test.cc
echo "set(libName $new_lib_name)

add_library(\${libName} \${libName}.h \${libName}.cc)
target_link_libraries(\${libName} common)

if(IS_TEST)
    add_executable(\${libName}_test \${libName}_test.cc)
    target_link_libraries(\${libName}_test test_common \${libName})

    gtest_discover_tests(\${libName}_test)
endif()" > $new_lib_dir/CMakeLists.txt

cdAndExit 0