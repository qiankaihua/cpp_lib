cmake_minimum_required(VERSION 3.14)

project(cpp_lib)

set(CMAKE_CXX_STANDARD 14)

option(IS_TEST "is test" OFF)
option(OPEN_O3 "enable o3 opt" OFF)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_BUILD_TYPE "Release")
if(IS_TEST)
    set(CMAKE_BUILD_TYPE "Debug")
endif()

message(STATUS "Compiling cpp lib")
# include_directories(BEFORE SYSTEM
#     deps/abseil-cpp
# )
if(IS_TEST)
    add_definitions(-DENABLE_ASAN)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fno-omit-frame-pointer")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0")

    message(STATUS "Use gcov")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")
else()
    # 开启 -O3 优化
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -W -Werror -Wall -Wextra -g")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -W -Werror -Wall -Wextra -g")

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--exclude-libs,ALL")
set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_C_VISIBILITY_PRESET hidden)

# 打印 message
message("CMAKE_C_FLAGS = ${CMAKE_C_FLAGS}")
message("CMAKE_CXX_FLAGS = ${CMAKE_CXX_FLAGS}")

include_directories(${PROJECT_SOURCE_DIR}
                    ${PROJECT_SOURCE_DIR}/src
)

add_subdirectory(deps)

if(IS_TEST)
    # NOTICE: 这是避免 gtest 编译的时候报 warning 的配置
    cmake_policy(SET CMP0135 NEW)
    include(FetchContent)

    FetchContent_Declare(
        googletest
        URL https://github.com/google/googletest/archive/f8d7d77c06936315286eb55f8de22cd23c188571.zip
    )
    include_directories(BEFORE
        deps/gtest
        deps/gtest/gtest)
    set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
    FetchContent_MakeAvailable(googletest)
    message(STATUS "Compiling ut")
    add_definitions(-DENABLE_TEST)
    enable_testing()

    include(GoogleTest)
    # ci 需要跑的测试可使用 gtest_discover_tests() 添加到列表中
endif()

if(EXISTS ${PROJECT_SOURCE_DIR}/src/main.cc)
    message("build main")
    add_executable(main src/main.cc)
    target_link_libraries(main
        ${BUILD_LIB_LIST}
    )
endif()

add_subdirectory(src)
