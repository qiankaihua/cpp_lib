#pragma once

#include <thread>

#include "Lock.h"

namespace Qkh_Lock {

class Condition {
public:
    QKH_LOCK_DISALLOW_COPY_AND_ASSIGN(Condition);
    explicit Condition(Lock* lock) {
        pthread_cond_init(&_cond, nullptr);
        _lock = lock;
    }
    virtual ~Condition() {
        pthread_cond_destroy(&_cond);
    }
    virtual int wait() {
        return pthread_cond_wait(&_cond, _lock->get_lock());
    }
    virtual int broadcast() {
        return pthread_cond_broadcast(&_cond);
    }
private:
    pthread_cond_t _cond;
    Lock *_lock;

};

}
