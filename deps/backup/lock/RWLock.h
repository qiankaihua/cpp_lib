#pragma once

#include <assert.h>
#include "Lock.h"

#include <iostream>
namespace Qkh_Lock {

class RWLock {
public:
    QKH_LOCK_DISALLOW_COPY_AND_ASSIGN(RWLock);
    explicit RWLock() {
        m_wc = m_rc = 0;
    }
    ~RWLock() {}

    inline void writer_enter() {
        LockGuard wclock(&m_wc_lock);
        ++m_wc;
        if (1 == m_wc) {
            m_r_lock.lock();
        }
        m_w_lock.lock();
    }
    inline void writer_leave() {
        m_w_lock.unlock();
        LockGuard wclock(&m_wc_lock);
        if (m_wc > 0) {
            --m_wc;
        }
        if (m_wc == 0) {
            m_r_lock.unlock();
        }
    }
    inline void reader_enter() {
        LockGuard rLock(&m_r_lock);
        LockGuard rcLock(&m_rc_lock);
        ++m_rc;
        if (1 == m_rc) {
            m_w_lock.lock();
        }
    }
    inline void reader_leave() {
        LockGuard rcLock(&m_rc_lock);
        if (m_rc > 0) {
            --m_rc;
        }
        if (0 == m_rc) {
            m_w_lock.unlock();
        }
    }
private:
    int m_wc, m_rc;
    Lock m_wc_lock, m_rc_lock, m_w_lock, m_r_lock;
};

class RwlockGuard {
public:
    enum CALLER_TYPE {
        WRITER = 0,
        READER = 1
    };
    RwlockGuard(RWLock* lock, CALLER_TYPE caller_type) {
        assert((CALLER_TYPE::WRITER == caller_type) || (CALLER_TYPE::READER == caller_type));
        m_rwlock = lock;
        m_caller_type = caller_type;
        switch (m_caller_type) {
        case CALLER_TYPE::READER:
            RwlockGuard::reader_enter(m_rwlock);
            break;
        case CALLER_TYPE::WRITER:
            RwlockGuard::writer_enter(m_rwlock);
        }
    }
    ~RwlockGuard() {
        switch (m_caller_type) {
        case CALLER_TYPE::READER:
            RwlockGuard::reader_leave(m_rwlock);
            break;
        case CALLER_TYPE::WRITER:
            RwlockGuard::writer_leave(m_rwlock);
        }
    }
    static inline void writer_enter(RWLock* lock) {
        lock->writer_enter();
    }
    static inline void writer_leave(RWLock* lock) {
        lock->writer_leave();
    }
    static inline void reader_enter(RWLock* lock) {
        lock->reader_enter();
    }
    static inline void reader_leave(RWLock* lock) {
        lock->reader_leave();
    }
private:
    RWLock* m_rwlock;
    CALLER_TYPE m_caller_type;
};

}