#pragma once

#include <functional>
#include <set>
#include <inttypes.h>
#include <iostream>

#include "Lock.h"
#include "Condition.h"

namespace Qkh_Lock {

template<typename KeyT>
class KeyLock {
public:
    QKH_LOCK_DISALLOW_COPY_AND_ASSIGN(KeyLock);
    explicit KeyLock() {
        _cond = new Condition(&_lock);
    }
    ~KeyLock() {
        delete _cond;
    }

    // template<typename KeyT>
    inline void lock(KeyT key) {
        LockGuard lock(&_lock);
        
        while (_set.find(key) != _set.end()) {
            int ret = _cond->wait();
            if (ret != 0) {
                std::cout << "cond wait failed, ret=" << ret << " errno=" << errno << std::endl;
                exit(-1);
                // LOG(FATAL) << "cond wait failed, ret=" << ret << " errno=" << errno;
                // while (0 != raise(SIGKILL)) {}
            }
        }
        _set.emplace(key);
    }
    inline bool tryLock(KeyT key) {
        LockGuard lock(&_lock);

        if (_set.find(key) != _set.end()) {
            return false;
        } else {
            _set.emplace(key);
            return true;
        }
    }
    inline void unlock(KeyT key) {
        LockGuard lock(&_lock);

        if (_set.find(key) != _set.end()) {
            _set.erase(key);
            int ret = _cond->broadcast();
            if (ret != 0) {
                std::cout << "cond broadcast failed, ret=" << ret << " errno=" << errno << std::endl;
                exit(-1);
                // LOG(FATAL) << "cond broadcast failed, ret=" << ret << " errno=" << errno;
                // while (raise(SIGKILL) != 0) {}
            }
        }
    }

private:
    Lock _lock;
    Condition *_cond;
    std::set<KeyT> _set;
};

template<typename KeyT>
class KeyLockGuard {
public:
    KeyLockGuard(KeyLock<KeyT>* lock, const KeyT key) {
        _lock = lock;
        _key = key;
        _lock->lock(_key);
    }
    ~KeyLockGuard() {
        _lock->unlock(_key);
    }
private:
    KeyLock<KeyT> *_lock;
    KeyT _key;
};

}
