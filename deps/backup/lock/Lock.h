#pragma once

#include <type_traits>
#include <thread>

namespace Qkh_Lock {

#define QKH_LOCK_DISALLOW_COPY_AND_ASSIGN(Typename) \
    Typename(const Typename&) = delete; \
    void operator=(const Typename&) = delete

class Lock {
public:
    QKH_LOCK_DISALLOW_COPY_AND_ASSIGN(Lock);
    Lock() {
        pthread_mutex_init(&_lock, nullptr);
    }
    ~Lock() {
        pthread_mutex_destroy(&_lock);
    }
    void lock() {
        pthread_mutex_lock(&_lock);
    }
    void unlock() {
        pthread_mutex_unlock(&_lock);
    }
    pthread_mutex_t* get_lock() {
        return &_lock;
    }
private:
    pthread_mutex_t _lock;
};

class LockGuard {
public:
    explicit LockGuard(Lock* lock) {
        _l = lock;
        _l->lock();
    }
    ~LockGuard() {
        _l->unlock();
    }
private:
    Lock *_l;
};

}