#pragma once

#include <string>
#include <stdio.h>

namespace q_cpp_lib {

inline int string_printf_impl(std::string& output, const char* format, va_list args) {
    const int write_point = output.size();
    int remaining = output.capacity() - write_point;
    output.resize(output.capacity());

    va_list copied_args;
    va_copy(copied_args, args);
    int bytes_used = vsnprintf(&output[write_point], remaining, format,
                               copied_args);
    va_end(copied_args);
    if (bytes_used < 0) {
        return -1;
    } else if (bytes_used < remaining) {
        // There was enough room, just shrink and return.
        output.resize(write_point + bytes_used);
    } else {
        output.resize(write_point + bytes_used + 1);
        remaining = bytes_used + 1;
        bytes_used = vsnprintf(&output[write_point], remaining, format, args);
        if (bytes_used + 1 != remaining) {
            return -1;
        }
        output.resize(write_point + bytes_used);
    }
    return 0;
}

inline int string_vprintf(std::string* output, const char* format, va_list args) {
    output->clear();
    const int rc = string_printf_impl(*output, format, args);
    if (rc == 0) {
        return 0;
    }
    output->clear();
    return rc;
}

inline void string_printf(std::string &input, const char * format, ...) {
    va_list arg;
    va_start(arg, format);
    string_vprintf(&input, format, arg);
    va_end(arg);
}

inline void string_appendf(std::string &input, const char * format, ...) {
    std::string _tmp_buf;
    va_list arg;
    va_start(arg, format);
    string_vprintf(&_tmp_buf, format, arg);
    va_end(arg);
    input += _tmp_buf;
}

} // namespace q_cpp_lib