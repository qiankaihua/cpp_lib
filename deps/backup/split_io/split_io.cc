#pragma once

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include "split_io.h"

namespace QSplitIO {

SplitIO::SplitIO() {
    _cur_file_num = 0;
    _cur_pos = 0;
    _split_size = DEFAULT_SPLIT_SIZE;
    _flags = 0;
    _path = "";

    for (int i = 0; i < MAX_SPLIT_FILE_NUM; ++i) {
        _fd[i] = -1;
    }
}

SplitIO::SplitIO(unsigned int split_size) {
    _cur_file_num = 0;
    _cur_pos = 0;
    _split_size = split_size;
    _flags = 0;
    _path = "";

    for (int i = 0; i < MAX_SPLIT_FILE_NUM; ++i) {
        _fd[i] = -1;
    }
}

SplitIO::~SplitIO() {
    for (int i = 0; i < _cur_file_num; ++i) {
        if (_fd[i] != -1) {
            ::close(_fd[i]);
        }
    }
}

bool SplitIO::open(const std::string& path, int flags) {
    if (path == "") {
        std::cout << "para invalid." << std::endl;
        return false;
    }
    if(flags & O_TRUNC){
        std::cout << "Split io can not use O_TRUNC option." << std::endl;
        return false;
    }
    _path = path;
    _flags = flags;
    std::string filename = "";
    int ret = 0;
    struct stat st;
    bool go_to_fail = false;
    int i = 0;
    for (; i < MAX_SPLIT_FILE_NUM; ++i) {
        filename = _path + "." + std::to_string(i);
        ret = stat(filename.c_str(), &st);
        if (ret == 0 && S_ISREG(st.st_mode)) {
            _fd[i] = ::open(filename.c_str(), flags, SplitIO::mode); 
            if (_fd[i] == -1) {
                std::cout << "SplitIO open file fail."
                    << " filename: " << filename << " err: " << errno << std::endl;
                go_to_fail = true;
                break;
            }
            ++_cur_file_num;   
        } else if (ret == 0) {
            std::cout << "SplitIO target exist and not file. filename: " << filename << std::endl;
            go_to_fail = true;
            break;
        } else {
            if (i == 0) {
                _fd[i] = ::open(filename.c_str(), flags, SplitIO::mode);
                if (_fd[i] == -1) {
                    std::cout << "SplitIO open file fail."
                        << " filename: " << filename << " err: " << errno << std::endl;
                    go_to_fail = true;
                    break;
                }
                ++_cur_file_num;
                break;
            } else {
                break;
            }
        }
    }
    
    // success
    if (!go_to_fail) {
        _cur_pos = 0;
        return true;
    }

    // fail
    for (int j = 0; j < i; ++j) {
        ::close(_fd[j]);
        _fd[j] = -1;
    }
    _cur_file_num = 0;   
    return false;

}

void SplitIO::close() {
    for (int j = 0; j < _cur_file_num; ++j) {
        if (_fd[j] != -1) {
            ::close(_fd[j]);
            _fd[j] = -1;
        }
    }
    _cur_file_num = 0;
    _cur_pos = 0;
}

bool SplitIO::del() {
    std::string filename = "";
    int ret = 0;
    for (int i = 0; i < _cur_file_num; ++i) {
        if (_fd[i] != -1) {
            ::close(_fd[i]);
            _fd[i] = -1;
        }
        filename = _path + "." + std::to_string(i);
        ret = remove(filename.c_str());
        if (ret != 0) {
            std::cout << "remove file fail. filename: " << filename << " err: " << errno << std::endl;
            return false;
        }
    } 
    _cur_file_num = 0;
    _cur_pos = 0;
    return true;
}

int64_t SplitIO::read(void* buf, int64_t count, int64_t offset) {
    if (buf == NULL) {
        std::cout << "para invalid" << std::endl;
        return -1;
    }

    int fileno = offset / _split_size;
    int64_t file_offset = offset % _split_size;
    
    if (fileno >= _cur_file_num) {
        std::cout << "read error, fileno=" << fileno << " _cur_file_num=" << _cur_file_num << std::endl;
        return 0;
    }

    int64_t bytes_left = count;
    int64_t bytes_read = 0;

    int64_t bytes_can_read = _split_size - file_offset;
    int64_t bytes_to_read = bytes_left > bytes_can_read ? bytes_can_read : bytes_left;
    
    int ret = ::pread(_fd[fileno], (char*)buf + bytes_read, bytes_to_read, file_offset);
    if (ret != bytes_to_read) {
        // 这里由于不同的使用场景处理方式不同，因此此处不打错误日志，由业务方进行判断
        return ret;
    } else {
        bytes_left -= ret;
        bytes_read += ret;
        
        while (bytes_left > 0) {
            ++fileno;
            file_offset = 0;
            if (fileno >= _cur_file_num) {
                break;
            }
            
            bytes_can_read = _split_size;
            bytes_to_read = bytes_left > bytes_can_read ? bytes_can_read : bytes_left;
            
            ret = ::pread(_fd[fileno], (char*)buf + bytes_read, bytes_to_read, file_offset);
            if (ret < 0) {
                std::cout << "read fail. fileno: " << fileno
                            << " file_offset: " << file_offset
                            << " err: " << errno << std::endl;
                return ret;
            }

            bytes_left -= ret;
            bytes_read += ret;
            
            if (ret !=  bytes_to_read) {
                break;
            }
        }
    }
    return bytes_read;
}

int64_t SplitIO::write(const void* buf, int64_t count, int64_t offset) {
    if (buf == NULL) {
        std::cout << "wirte para invalid.." << std::endl;
        return -1;
    }
    if (_cur_file_num <= 0) {
        std::cout << "no file opened." << std::endl;
        return -1;
    }
    
    int ret;
    int fileno_start = offset / _split_size;
    int fileno_end = (offset + count) / _split_size;
    if (fileno_end >= MAX_SPLIT_FILE_NUM) {
        std::cout << "offset exceed limit. offset: " << offset << std::endl;
        return -1;
    }
    
    if (fileno_end >= _cur_file_num) {
        std::string filename = "";
        for (int i = _cur_file_num; i <= fileno_end; ++i) {
            filename = _path + "." + std::to_string(i);
            int flags = _flags | O_CREAT;
            _fd[i] = ::open(filename.c_str(), flags, SplitIO::mode);
            if (_fd[i] < 0) {
                std::cout << "open file fail."
                    << " filename: " << filename << " err: " << errno << std::endl;
                return -1;  
            }
            ++_cur_file_num;
        }
    }
    
    int64_t bytes_writen = 0;
    int fileno = fileno_start;
    while (bytes_writen < count) {
        int64_t bytes_left = count - bytes_writen;    

        int64_t bytes_can_write;
        int64_t file_offset;
        if (fileno == fileno_start) {
            file_offset = offset % _split_size;
            bytes_can_write = _split_size - file_offset;
        } else {
            file_offset = 0;
            bytes_can_write = _split_size;
        }
        int64_t bytes_to_write = bytes_left > bytes_can_write ? bytes_can_write : bytes_left;
        ret = ::pwrite(_fd[fileno], (char*)buf + bytes_writen,  bytes_to_write, file_offset);
        if (ret != bytes_to_write) {
            std::cout << "write file fail."
                << " fileno: " << fileno
                << " bytes_to_write: " << bytes_to_write
                << " ret: " << ret
                << " err: " << errno << std::endl;
            return -1;
        }
        ++fileno;
        bytes_writen += ret;
    }
    return bytes_writen;
}

int64_t SplitIO::read(void* buf, int64_t count) {
    Qkh_Lock::LockGuard lock(&_lock);
    int64_t ret = this->read(buf, count, _cur_pos);
    if (ret >= 0) {
        _cur_pos += ret;
        return ret;
    } else {
        std::cout << "read fail. offset: " << _cur_pos << std::endl;
        return ret;
    }
}

int64_t SplitIO::write(const void* buf, int64_t count) {
    Qkh_Lock::LockGuard lock(&_lock);
    int64_t ret = this->write(buf, count, _cur_pos);
    if (ret >= 0) {
        _cur_pos += ret; 
        return ret;  
    } else {
        std::cout << "write fail. offset: " << _cur_pos << std::endl;
        return ret;
    }
}

void SplitIO::flush() {
    for (int i = 0; i < _cur_file_num; ++i) {
        ::fsync(_fd[i]);
    }
}

int64_t SplitIO::size() {
    if (_cur_file_num == 0) {
        return 0;
    }

    std::string filename = _path + "." + std::to_string(_cur_file_num - 1);
    
    struct stat st;
    if (stat(filename.c_str(), &st) == 0) {
        return (int64_t)st.st_size + (int64_t)(_cur_file_num - 1) * (int64_t)(_split_size);
    } else {
        std::cout << "last file not found. "
            << "filename: " << filename << " err: " << errno << std::endl;
        return -1;
    }
}

int SplitIO::get_stat(struct stat *buf) {
    if (_cur_file_num == 0) {
        std::cout << "no file opened" << std::endl;
        return -1;
    }

    std::string filename = _path + ".0";
    if(stat(filename.c_str(), buf) == 0){
        return 0;
    } else {
        std::cout << "file not found. filename: " << filename << " err: " << errno << std::endl;
        return -1;
    }
}

int64_t SplitIO::seek(int64_t offset, int whence) {
    Qkh_Lock::LockGuard lock(&_lock);

    if (whence == SEEK_SET) {
        _cur_pos = offset;
    } else if (whence == SEEK_END) {
        int64_t size = this->size();
        if (size < 0) {
            std::cout << "size invalid." << std::endl;
            return -1;
        }
        _cur_pos = size - offset;
    } else if (whence == SEEK_CUR) {
        _cur_pos += offset;
    } else {
        std::cout << "flag error, get=" << whence << std::endl;
        return -1;
    }
    return 0;
}

}