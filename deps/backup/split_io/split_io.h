#pragma once

#include <string>
#include <memory>
#include <string>
#include <fcntl.h>

#include "../lock/Lock.h"

namespace QSplitIO {

class SplitIO {
public:
    const static int MAX_SPLIT_FILE_NUM = 1024;                 // 最大文件数量
    const static int DEFAULT_SPLIT_SIZE = 1024 * 1024 * 1024;   // 默认一个文件的大小 1G
public:
    SplitIO();
    /**
     * split_size : 划分单文件大小
     */
    SplitIO(unsigned int split_size);
    ~SplitIO();
    
    bool open(const std::string& path, int flags);
    void close();
    bool del();
    int64_t read(void* buf, int64_t count, int64_t offset);
    int64_t write(const void* buf, int64_t count, int64_t offset);
    int64_t read(void* buf, int64_t count);
    int64_t write(const void* buf, int64_t count);
    void flush();
    int64_t size();
    int get_stat(struct stat *buf);
    int64_t seek(int64_t offset, int whence);
    int64_t current_pos() { return _cur_pos; }
private:
    static const mode_t mode = 0664;    // 创建文件权限
    int _fd[MAX_SPLIT_FILE_NUM];        // 打开文件的句柄
    int _cur_file_num;                  // 当前文件数
    int64_t _cur_pos;                   // 当前位置
    unsigned int _split_size;           // 单文件大小
    int _flags;                         // 打开文件的标记位
    std::string _path;                  // 文件的路径

    Qkh_Lock::Lock _lock;     // 锁
};

}