#pragma once

#include <iostream>
#include <string>
#include <chrono>

namespace q_cpp_lib {

class SubTestLog_ {
 public:
    explicit SubTestLog_(std::string name) : name_(name) {
        auto prefix = get_prefix();
        std::cout << prefix << " >>> " << name_ << std::endl;
        ++SubTestLog_::now_level_;
        start = std::chrono::high_resolution_clock::now();
    }
    ~SubTestLog_() {
        --SubTestLog_::now_level_;
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::high_resolution_clock::now() - start);
        auto cost_us = duration.count();
        double true_cost;
        std::string cost_unit;
        if (cost_us > 1000000) {
            true_cost = (cost_us - cost_us % 100000) / 1000000.0;
            cost_unit = "s";
        } else if (cost_us > 1000) {
            true_cost = (cost_us - cost_us % 100) / 1000.0;
            cost_unit = "ms";
        } else {
            true_cost = cost_us;
            cost_unit = "us";
        }
        
        std::cout << get_prefix() << " <<< " << name_
            << ", cost " << true_cost << cost_unit << std::endl;
    }
    std::string name_;
 private:
    std::string get_prefix() {
        std::string prefix = "";
        for (auto i = 0; i < SubTestLog_::now_level_; ++i) {
            prefix += "│  ";
        }
        return prefix + "├──";
    }
 private:
#ifdef __APPLE__
    std::chrono::steady_clock::time_point  start;
#else
    std::chrono::_V2::system_clock::time_point start;
#endif
    thread_local static int now_level_;
};
thread_local int SubTestLog_::now_level_ = 0;

#define SubTestLog(name) auto _##__LINE__ = SubTestLog_(name)

} // namespace q_cpp_lib

