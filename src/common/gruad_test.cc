#include "common/gruad.h"

#include <gtest/gtest.h>

#include "test_common/test_common.h"

namespace q_cpp_lib {

TEST(GruadTest, BasicTest) {
    int check_point = 0, check_point2 = 0;
    {
        SubTestLog("gruad base");
        ASSERT_EQ(check_point, 0);
        ScopeGuard([&check_point]() {
            ASSERT_EQ(check_point, 2);
            check_point = 1;
        });
        ScopeGuard([&check_point]() {
            ASSERT_EQ(check_point, 0);
            check_point = 2;
        });
        ScopeGuard([&check_point2]() {
            ASSERT_EQ(check_point2, 0);
            check_point2 = 2;
        });
        // ScopeGuard a();
        ASSERT_EQ(check_point, 0);
    }
    ASSERT_EQ(check_point, 1);
    ASSERT_EQ(check_point2, 2);
}

} // namespace q_cpp_lib
