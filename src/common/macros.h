#pragma once

namespace q_cpp_lib {

#if defined(__GNUC__)
#define LIKELY(x) (__builtin_expect((x), 1))
#define UNLIKELY(x) (__builtin_expect((x), 0))
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif

#undef DISALLOW_COPY_AND_ASSIGN
#define DISALLOW_COPY_AND_ASSIGN(clazz) \
    clazz(const clazz&); \
    clazz& operator=(const clazz&); \
    clazz&& operator=(const clazz&&)

#undef CONCAT_IMPL
#define CONCAT_IMPL( x, y ) x##y
#undef MACRO_CONCAT
#define MACRO_CONCAT( x, y ) CONCAT_IMPL( x, y )

#ifdef ENABLE_TEST
#define NEED_TEST_FUNC __attribute__((__used__))
#else
#define NEED_TEST_FUNC
#endif

} // namespace q_cpp_lib
