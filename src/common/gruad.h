#pragma once

#include <functional>

#include "common/macros.h"

namespace q_cpp_lib {

class ScopeGuardClass {
 public:
    NEED_TEST_FUNC explicit ScopeGuardClass(std::function<void()> closure): function(std::move(closure)) {}
    NEED_TEST_FUNC ~ScopeGuardClass() {
        function();
    }

 private:
    std::function<void()> function;

    DISALLOW_COPY_AND_ASSIGN(ScopeGuardClass);
};
// #define __ScopeGuardLine(function, line) ScopeGuardClass __ScopeGuardClass_ ## line (function)
#define ScopeGuard(function) ScopeGuardClass MACRO_CONCAT(__ScopeGuardClass_, __COUNTER__)(function)

} // namespace q_cpp_lib
