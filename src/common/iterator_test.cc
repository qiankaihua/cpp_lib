#include "common/iterator.h"

#include <gtest/gtest.h>

#include "test_common/test_common.h"

namespace q_cpp_lib {
struct test {
    int a;
};
class iterator: public common::IteratorFacade<iterator, test, std::bidirectional_iterator_tag> { // 双向 tag 
 public:
    typedef test value_type;
    typedef value_type& reference;

    explicit iterator(test* value): value_(value) {}
 private:
    friend class common::IteratorFacade<iterator, test, std::bidirectional_iterator_tag>;

    value_type& dereference() const { return *value_; }
    bool equal(const iterator& other) const { return value_ == other.value_; }
    void increment() { value_++; }
    void decrement() { value_--; }
 private:
    test *value_;
};

TEST(IteratorTest, BasicTest) {
    {
        SubTestLog("iterator base");
        test test_int_array[10] = {{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}};
        iterator iter(&test_int_array[5]);
        ASSERT_EQ((*iter).a, 5);
        ASSERT_EQ(iter->a, 5);
        auto iter2 = iter++;
        ASSERT_EQ(iter->a, 6);
        ASSERT_EQ(iter2->a, 5);
        iter2 = iter--;
        ASSERT_EQ(iter->a, 5);
        ASSERT_EQ(iter2->a, 6);
        iter2 = ++iter;
        ASSERT_EQ(iter->a, 6);
        ASSERT_EQ(iter2->a, 6);
        iter2 = --iter;
        ASSERT_EQ(iter->a, 5);
        ASSERT_EQ(iter2->a, 5);
        ASSERT_EQ(iter2, iter);
        ASSERT_EQ(iter2, (++(--iter)));
    }
}

} // namespace q_cpp_lib
