#include "common/alloc.h"

#include <gtest/gtest.h>

#include "test_common/test_common.h"
#include "common/gruad.h"

namespace q_cpp_lib {

struct test {
    int a;
    int b;
};

TEST(AllocTest, BasicTest) {
    int64_t base_size = sizeof(Allocator);
    {
        SubTestLog("alloc base");
        auto alloc = Allocator::NewAlloc();
        ScopeGuard([&alloc](){
            Allocator::DeleteAlloc(alloc);
        });
        Allocator::DeleteAlloc(nullptr);
        ASSERT_EQ(alloc->Size(), base_size);
        void *p = alloc->Allocate(123);
        ASSERT_EQ(alloc->Size(), base_size + 123);
        alloc->Deallocate(p, 123);
        ASSERT_EQ(alloc->Size(), base_size);
    }
    {
        SubTestLog("alloc global");
        ASSERT_EQ(Allocator::GlobalAlloc()->Size(), base_size);
        void *p = Allocator::GlobalAlloc()->Allocate(123);
        ASSERT_EQ(Allocator::GlobalAlloc()->Size(), base_size + 123);
        Allocator::GlobalAlloc()->Deallocate(p, 123);
        ASSERT_EQ(Allocator::GlobalAlloc()->Size(), base_size);
    }
    {
        SubTestLog("stl alloc base");
        auto stl_alloc = Allocator::GlobalAlloc()->GetStlWrapper<test>();
        test* p = (test*)stl_alloc.allocate(1);
        ASSERT_NE(p, nullptr);
        ASSERT_EQ(Allocator::GlobalAlloc()->Size(), base_size + int64_t(sizeof(test)));
        ASSERT_EQ(stl_alloc.address(*p), p);
        const test t{.a = 1, .b = 2};
        ASSERT_EQ(stl_alloc.address(t), &t);
        ASSERT_EQ(stl_alloc.const_address(t), &t);
        ASSERT_GT(stl_alloc.max_size(), 0ul);
        stl_alloc.destory(p);
        ASSERT_EQ(Allocator::GlobalAlloc()->Size(), base_size + int64_t(sizeof(test)));
        stl_alloc.deallocate(p, 1);
        ASSERT_EQ(Allocator::GlobalAlloc()->Size(), base_size);
    }
}

} // namespace q_cpp_lib
