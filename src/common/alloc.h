#pragma once

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <cstddef>
#include <limits>
#include <memory>
#include <utility>
#ifdef ENABLE_TEST
#include <iostream>
#endif
#include <cstdlib>

#include "common/macros.h"

namespace q_cpp_lib {

class Allocator {
 public:
    template <typename T>
    class StlWrapper;

    NEED_TEST_FUNC explicit Allocator() {
        size_ = sizeof(Allocator);
    }
    NEED_TEST_FUNC ~Allocator() {
        #ifdef ENABLE_TEST
        std::cout << "alloc destroy: " << size_ << std::endl << std::flush;
        #endif
    }
    NEED_TEST_FUNC Allocator(Allocator&&) = delete;

    NEED_TEST_FUNC void* Allocate(size_t size) {
        #ifdef ENABLE_TEST
        std::cout << "alloc = " << size << std::endl;
        #endif
        size_ += size;
        return malloc(size);
    }

    NEED_TEST_FUNC void Deallocate(void* p, size_t size) {
        #ifdef ENABLE_TEST
        std::cout << "free = " << size << std::endl;
        #endif
        size_ -= size;
        free(p);
    }

    template <typename T>
    NEED_TEST_FUNC StlWrapper<T> GetStlWrapper() {
        return StlWrapper<T>(this);
    }
    NEED_TEST_FUNC int64_t Size() const {
        return size_;
    }

    NEED_TEST_FUNC static Allocator* GlobalAlloc() {
        static Allocator global_alloc;
        return &global_alloc;
    }
    NEED_TEST_FUNC static Allocator* NewAlloc() {
        return new (malloc(sizeof(Allocator))) Allocator();
    }
    NEED_TEST_FUNC static void DeleteAlloc(Allocator* alloc) {
        if (LIKELY(alloc != nullptr)) {
            alloc->~Allocator();
            free(alloc);
        }
        return;
    }

    DISALLOW_COPY_AND_ASSIGN(Allocator);
 private:
    int64_t size_;

};

// for adapts std::allocator
template <typename T>
class Allocator::StlWrapper {
 public:
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;
    using value_type = T;

    using propagate_on_container_move_assignment = std::true_type;
    using propagate_on_container_copy_assignment = std::true_type;
    using propagate_on_container_swap = std::true_type;
    using is_always_equal = std::false_type;

    template <typename U>
    struct rebind {
        using other = StlWrapper<U>;
    };

    NEED_TEST_FUNC StlWrapper() : impl_(Allocator::GlobalAlloc()) {}
    NEED_TEST_FUNC explicit StlWrapper(Allocator* impl) : impl_(impl) {}

    template <typename U>
    NEED_TEST_FUNC StlWrapper(const StlWrapper<U>& other) : impl_(other.Impl()) {}

    NEED_TEST_FUNC pointer address(reference x) const { return std::addressof(x); }

    NEED_TEST_FUNC const_pointer address(const_reference x) const { return std::addressof(x); }
    NEED_TEST_FUNC const_pointer const_address(const_reference x) const { return std::addressof(x); }

    NEED_TEST_FUNC pointer allocate(size_type n) {
        return reinterpret_cast<pointer>(impl_->Allocate(n * sizeof(T)));
    }

    NEED_TEST_FUNC void deallocate(pointer p, size_type n) { return impl_->Deallocate(p, n * sizeof(T)); }
    
    // pointer reallocate(pointer p, size_type n) {
    //     return reinterpret_cast<pointer>(impl_->Reallocate(p, n * sizeof(T)));
    // }

    NEED_TEST_FUNC size_type max_size() const { return std::numeric_limits<size_type>::max() / sizeof(T); }

    template <class... Args>
    NEED_TEST_FUNC void construct(pointer p, Args&&... args) {
        ::new (p) T(std::forward<Args>(args)...);
    }
    NEED_TEST_FUNC void destory(pointer p) {
        p->~T();
    }

    template <class U, class... Args>
    NEED_TEST_FUNC void construct(U* p, Args&&... args) {
        ::new (p) U(std::forward<Args>(args)...);
    }

    template <typename U>
    NEED_TEST_FUNC void destroy(U* p) {
        p->~U();
    }

    NEED_TEST_FUNC Allocator* Impl() const { return impl_; }

 private:
    Allocator* impl_ = nullptr;
};

template <class T1, class T2>
NEED_TEST_FUNC bool operator!=(const Allocator::StlWrapper<T1>& lhs, const Allocator::StlWrapper<T2>& rhs) {
    return lhs.Impl() != rhs.Impl();
}

template <class T1, class T2>
NEED_TEST_FUNC bool operator==(const Allocator::StlWrapper<T1>& lhs, const Allocator::StlWrapper<T2>& rhs) {
    return lhs.Impl() == rhs.Impl();
}

} // namespace q_cpp_lib
