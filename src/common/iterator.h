#pragma once

#include <cstddef>
#include <iterator>
#include <type_traits>

#include "common/macros.h"

namespace q_cpp_lib {
namespace common {

// D is Drive of Data
// need func:
// D.dereference
// D.increment
// D.decrement
// D.equal
template <class D, class V, class Tag>
class IteratorFacade {
 public:
    using value_type = V;
    using reference = value_type&;
    using pointer = value_type*;
    using difference_type = std::ptrdiff_t;
    using iterator_category = Tag;

    NEED_TEST_FUNC friend bool operator==(D const& lhs, D const& rhs) { return equal(lhs, rhs); }

    NEED_TEST_FUNC friend bool operator!=(D const& lhs, D const& rhs) { return !(lhs == rhs); }

    NEED_TEST_FUNC V& operator*() const { return asDerivedConst().dereference(); }

    NEED_TEST_FUNC V* operator->() const { return std::addressof(operator*()); }

    NEED_TEST_FUNC D& operator++() {
        asDerived().increment();
        return asDerived();
    }

    NEED_TEST_FUNC D operator++(int) {
        auto ret = asDerived(); // copy
        asDerived().increment();
        return ret;
    }
    template <typename T = Tag>
    NEED_TEST_FUNC typename std::enable_if<!std::is_same<T, std::forward_iterator_tag>::value, D&>::type operator--() {
        asDerived().decrement();
        return asDerived();
    }
    template <typename T = Tag>
    NEED_TEST_FUNC typename std::enable_if<!std::is_same<T, std::forward_iterator_tag>::value, D>::type operator--(int) {
        auto ret = asDerived(); // copy
        asDerived().decrement();
        return ret;
    }

 private:
    NEED_TEST_FUNC D& asDerived() { return static_cast<D&>(*this); }

    NEED_TEST_FUNC D const& asDerivedConst() const { return static_cast<D const&>(*this); }

    NEED_TEST_FUNC static bool equal(D const& lhs, D const& rhs) { return lhs.equal(rhs); }
};

} // namespace common
} // namespace q_cpp_lib
