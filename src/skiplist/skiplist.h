#pragma once

#include <iostream>
#include <random>
#include <type_traits>
#include <vector>
#include <utility>
#include <time.h>
#include <stdint.h>

#ifdef ENABLE_TEST
#include <gtest/gtest_prod.h>
#endif

#include "common/iterator.h"
#include "common/macros.h"

namespace q_cpp_lib {
namespace skiplist {

#define K_SKIPLIST_DEFAULT_MAX_LEVEL 32
#define K_SKIPLIST_DEFAULT_P 0.25

template <typename ValueT, typename NodeT>
class iterator;

template <typename T>
class Node;

template <typename T, typename Compare = std::less<T>, typename Allocator = std::allocator<T>, uint8_t MAX_LEVEL = K_SKIPLIST_DEFAULT_MAX_LEVEL>
class skiplist {
 private:
    using NodeType = Node<T>;
 public:
    using iter_type = iterator<T, NodeType>;
    using const_iter_type = iterator<const T, NodeType>;

 public:
    static_assert(MAX_LEVEL > 0, "MAX_LEVEL must be greater than 0");
    NEED_TEST_FUNC explicit skiplist(Allocator a = Allocator()) { init(K_SKIPLIST_DEFAULT_P, a); }
    NEED_TEST_FUNC explicit skiplist(double p_for_one_level, Allocator a = Allocator()) { init(p_for_one_level, a); }
    NEED_TEST_FUNC ~skiplist() {
        Node<T> *tmp_node, *now = skip_;
        while (now) {
            tmp_node = now->next();
            NodeType::destroy(alloc_, now);
            now = tmp_node;
        }
    }
    NEED_TEST_FUNC iter_type find(const T& data) {
        Node<T>* now = skip_;
        for (int i = skip_->max_layer(); i >= 0; --i) {
            auto nxt = now->skip(i);
            while (nxt != nullptr && less(nxt->data(), data)) {
                now = nxt;
                nxt = now->skip(i);
            }
            if (nxt != nullptr && equal(nxt->data(), data)) {
                return iter_type(nxt);
            }
        }
        return end();
    }
    NEED_TEST_FUNC bool del(const T& data) {
        Node<T>* now = skip_;
        Node<T>* node = nullptr;
        bool deleted = false;
        for (int i = skip_->max_layer(); i >= 0; --i) {
            auto nxt = now->skip(i);
            while (nxt != nullptr && less(nxt->data(), data)) {
                now = nxt;
                nxt = now->skip(i);
            }
            if (nxt != nullptr && equal(nxt->data(), data)) {
                now->set_skip(i, nxt->skip(i));
                deleted = true;
                node = nxt;
            }
        }
        if (node != nullptr) {
            NodeType::destroy(alloc_, node);
            --count_;
        }
        return deleted;
    }
    // first >=
    NEED_TEST_FUNC iter_type lower_bound(T&& data) {
        Node<T>* now = skip_;
        for (int i = skip_->max_layer(); i >= 0; --i) {
            auto nxt = now->skip(i);
            while (nxt != nullptr && less(nxt->data(), data)) {
                now = nxt;
                nxt = now->skip(i);
            }
            if (nxt != nullptr && equal(nxt->data(), data)) {
                return iter_type(nxt);
            }
        }
        if (now->skip(0) == nullptr) {
            return end();
        }
        return iter_type(now->skip(0));
    }
    // first >
    NEED_TEST_FUNC iter_type upper_bound(T&& data) {
        Node<T>* now = skip_;
        for (int i = skip_->max_layer(); i >= 0; --i) {
            auto nxt = now->skip(i);
            while (nxt != nullptr && !less(data, nxt->data())) {
                now = nxt;
                nxt = now->skip(i);
            }
        }
        if (now->skip(0) == nullptr) {
            return end();
        }
        return iter_type(now->skip(0));
    }
    NEED_TEST_FUNC std::pair<iter_type, bool> put(T&& data) {
        bool found = false;
        Node<T>* now = skip_;
        std::vector<Node<T>*> prev(skip_->height(), skip_);
        for (int i = skip_->max_layer(); i >= 0; --i) {
            auto nxt = now->skip(i);
            while (nxt != nullptr && less(nxt->data(), data)) {
                now = nxt;
                nxt = now->skip(i);
            }
            if (nxt != nullptr && equal(nxt->data(), data)) {
                found = true;
                now = nxt;
                break;
            }
            prev[i] = now;
        }
        if (!found) {
            ++count_;
            auto new_node = NodeType::create(alloc_, get_height(), std::forward<T>(data));
            int now_layer = 0, max_layer = new_node->height() < prev.size() ? new_node->height() : prev.size();
            for (; now_layer < max_layer; ++now_layer) {
                new_node->set_skip(0, prev[now_layer]->skip(now_layer));
                prev[now_layer]->set_skip(now_layer, new_node);
            }
            for (; now_layer < new_node->height(); ++now_layer) {
                new_node->set_skip(now_layer, skip_->skip(now_layer));
                skip_->set_skip(now_layer, new_node);
            }
            return {iter_type(new_node), true};
        } else {
            now->set_data(std::forward<T>(data));
            return {iter_type(now), false};
        }
    }
    NEED_TEST_FUNC size_t size() const { return count_; }
    NEED_TEST_FUNC bool empty() const { return count_ == 0; }
    NEED_TEST_FUNC iter_type begin() const { return iter_type(skip_->skip(0)); }
    NEED_TEST_FUNC iter_type end() const { return iter_type(nullptr); }
    NEED_TEST_FUNC const_iter_type cbegin() const { return begin(); }
    NEED_TEST_FUNC const_iter_type cend() const { return end(); }
    #ifdef Q_DEBUG_SKIP_LIST
    void print(std::ostream& os = std::cout) {
        for (int i = skip_->max_layer(); i >= 0; --i) {
            auto now = skip_->skip(i);
            os << "|level " << i << ":head|";
            while (now != nullptr) {
                os << "---->";
                os << '|' << now->data() << '|';
                now = now->skip(i);
            }
            os << std::endl;
        }
    }
    #endif

    DISALLOW_COPY_AND_ASSIGN(skiplist);
 private:
    typedef typename Allocator::template rebind<uint8_t>::other u8Alloc;

    NEED_TEST_FUNC void init(double p, Allocator a) {
        count_ = 0;
        alloc_ = a;
        double kp = 1.0;
        for (int i = 0; i < MAX_LEVEL; ++i) {
            kp *= p;
            lookup_p_[i] = 1 - kp;
        }
        skip_ = NodeType::create(alloc_, MAX_LEVEL, T());
    }
    NEED_TEST_FUNC static bool less(const T& lhs, const T& rhs) {
        return Compare()(lhs, rhs);
    }
    NEED_TEST_FUNC static bool equal(const T& lhs, const T& rhs) {
        return !less(lhs, rhs) && !less(rhs, lhs);
    }
    NEED_TEST_FUNC uint8_t get_height() {
        auto p = rand();
        for (uint8_t i = 0; i < MAX_LEVEL; ++i) {
            if (p < lookup_p_[i]) {
                return i + 1;
            }
        }
        return MAX_LEVEL;
    }
    NEED_TEST_FUNC double rand() {
        static std::default_random_engine rand_engine(time(nullptr));
        static std::uniform_real_distribution<double> u(0.0, 1.0);
        return u(rand_engine);
    }

 private:
    #ifdef ENABLE_TEST
        // gtest friend
        FRIEND_TEST(SkiplistTest, BasicTest);
    #endif
    double lookup_p_[MAX_LEVEL];
    Node<T>* skip_;
    size_t count_;
    u8Alloc alloc_;
};

template <typename T>
class Node {
 public:
    template <typename Allocator, typename U, typename = typename std::enable_if<std::is_convertible<U, T>::value>::type>
    NEED_TEST_FUNC static Node* create(Allocator& alloc, uint8_t height, U&& data) {
        size_t size = sizeof(Node) + height * sizeof(Node*);
        auto storage = std::allocator_traits<Allocator>::allocate(alloc, size);
        return new (storage) Node(uint8_t(height), std::forward<U>(data));
    }
    template <typename Allocator>
    NEED_TEST_FUNC static void destroy(Allocator& alloc, Node* node) {
        size_t size = sizeof(Node) + node->height_ * sizeof(Node*);
        node->~Node();
        std::allocator_traits<Allocator>::deallocate(alloc, typename std::allocator_traits<Allocator>::pointer(node), size);
    }

    NEED_TEST_FUNC T& data() { return data_; }
    NEED_TEST_FUNC const T& data() const { return data_; }
    NEED_TEST_FUNC Node* next() { return skip(0); }
    NEED_TEST_FUNC inline Node* skip(int layer) const {
        return skip_[layer];
    }
    NEED_TEST_FUNC void set_skip(int layer, Node* node) { skip_[layer] = node; }
    NEED_TEST_FUNC void set_data(T&& data) { data_ = data; }
    NEED_TEST_FUNC int max_layer() const { return height_ - 1; }
    NEED_TEST_FUNC uint8_t height() const { return height_; }

    DISALLOW_COPY_AND_ASSIGN(Node);
 private:
    template <typename U>
    NEED_TEST_FUNC explicit Node(uint8_t height, U&& data): height_(height), data_(std::forward<U>(data)) {
        for (uint8_t i = 0; i < height_; ++i) {
            skip_[i] = nullptr;
        }
    }
    NEED_TEST_FUNC ~Node() {}

 private:
    #ifdef ENABLE_TEST
        // gtest friend
        FRIEND_TEST(SkiplistTest, BasicTest);
    #endif
    uint8_t height_;
    T data_;
    Node* skip_[0];
};

template <typename ValueT, typename NodeT>
class iterator: public common::IteratorFacade<iterator<ValueT, NodeT>, ValueT, std::forward_iterator_tag> {
 public:
    typedef ValueT value_type;
    // typedef value_type node_type;
    typedef value_type& reference;
    typedef value_type* pointer;
    
    template <typename ValueU, typename NodeU>
    NEED_TEST_FUNC friend std::ostream& operator<<(std::ostream& os, const iterator<ValueU, NodeU>& p);

    NEED_TEST_FUNC explicit iterator(NodeT* node = nullptr): node_(node) {}
    template <typename OtherVal, typename OtherNode>
    NEED_TEST_FUNC iterator(
        const iterator<OtherVal, OtherNode>& other,
        typename std::enable_if<std::is_convertible<OtherVal*, ValueT*>::value>::type* = nullptr
    ): node_(other.node_) {}
 private:
    template <class, class>
    friend class iterator;
    friend class common::IteratorFacade<iterator<ValueT, NodeT>, ValueT, std::forward_iterator_tag>;

    NEED_TEST_FUNC value_type& dereference() const { return node_->data(); }
    NEED_TEST_FUNC bool equal(const iterator& other) const { return node_ == other.node_; }
    NEED_TEST_FUNC void increment() {
        if (UNLIKELY(node_ == nullptr)) {
            return;
        }
        node_ = node_->next();
    }
 private:
    #ifdef ENABLE_TEST
        // gtest friend
        FRIEND_TEST(SkiplistTest, BasicTest);
    #endif
    NodeT* node_;
};
template <typename ValueT, typename NodeT>
NEED_TEST_FUNC std::ostream& operator<<(std::ostream& os, const iterator<ValueT, NodeT>& p) {
    if (p.node_) { os << p.dereference(); }
    else { os << "[empty node]"; }
    return os;
}

} // namespace skiplist
} // namespace q_cpp_lib
