#include "skiplist/skiplist.h"

#include <gtest/gtest.h>
#include <sstream>

#include "test_common/test_common.h"
#include "common/alloc.h"
#include "common/gruad.h"

namespace q_cpp_lib {
namespace skiplist {

struct test {
    int a;
    int b;

    friend std::ostream& operator<<(std::ostream& os, const test& p);
};
std::ostream& operator<<(std::ostream& os, const test& p) {
    os << "{a: "<<p.a << ", b: "<<p.b<<"}";
    return os;
}

struct MyLess {
    bool operator()(const test& a, const test& b) const {
        // 在这里实现自定义的比较逻辑
        return a.a < b.a;
    }
};

TEST(SkiplistTest, BasicTest) {
    {
        SubTestLog("skiplist base");
        skiplist<test, MyLess> sk(0.5);
        ASSERT_EQ(sk.begin().node_, nullptr);
        ASSERT_EQ(sk.end().node_, nullptr);
        ASSERT_EQ(sk.cbegin().node_, nullptr);
        ASSERT_EQ(sk.cend().node_, nullptr);
        ASSERT_EQ(sk.size(), 0u);
        ASSERT_TRUE(sk.empty());

        for (int i = 0; i < 100; ++i) {
            test data = test{.a = i, .b = i};
            auto ins = sk.put(std::move(data));
            ASSERT_TRUE(ins.second);
            ASSERT_EQ(ins.first->a, i);
            ASSERT_EQ(ins.first->b, i);
            ASSERT_EQ(sk.size(), size_t(i+1));
            ins = sk.put(test{.a = i, .b = i + 1});
            ASSERT_FALSE(ins.second);
            ASSERT_EQ(ins.first->a, i);
            ASSERT_EQ(ins.first->b, i + 1);
            ASSERT_EQ(sk.size(), size_t(i+1));
        }
        ASSERT_FALSE(sk.empty());
        ASSERT_NE(sk.begin().node_, nullptr);
        ASSERT_EQ(sk.end().node_, nullptr);
        ASSERT_NE(sk.cbegin().node_, nullptr);
        ASSERT_EQ(sk.cend().node_, nullptr);

        auto iter = sk.find(test{.a = 5, .b = 0});
        ASSERT_NE(iter, sk.end());
        ASSERT_EQ(iter->a, 5);
        ASSERT_EQ(iter->b, 6);
        iter++;
        ASSERT_NE(iter, sk.end());
        ASSERT_EQ(iter->a, 6);
        ASSERT_EQ(iter->b, 7);
        iter = sk.find(test{.a = 100, .b = 0});
        ASSERT_EQ(iter, sk.end());
        iter++;
        ASSERT_EQ(iter, sk.end());

        iter = sk.lower_bound(test{.a = 0, .b = 0});
        ASSERT_NE(iter, sk.end());
        ASSERT_EQ(iter, sk.begin());
        ASSERT_EQ(iter->a, 0);
        ASSERT_EQ(iter->b, 1);
        iter = sk.lower_bound(test{.a = 5, .b = 0});
        ASSERT_NE(iter, sk.end());
        ASSERT_EQ(iter->a, 5);
        ASSERT_EQ(iter->b, 6);
        iter = sk.lower_bound(test{.a = 100, .b = 0});
        ASSERT_EQ(iter, sk.end());

        iter = sk.upper_bound(test{.a = 5, .b = 0});
        ASSERT_NE(iter, sk.end());
        ASSERT_EQ(iter->a, 6);
        ASSERT_EQ(iter->b, 7);
        iter = sk.upper_bound(test{.a = 100, .b = 0});
        ASSERT_EQ(iter, sk.end());

        ASSERT_TRUE(sk.del(test{.a = 5, .b = 0}));
        ASSERT_FALSE(sk.del(test{.a = 5, .b = 0}));
        ASSERT_FALSE(sk.del(test{.a = 100, .b = 0}));
        ASSERT_EQ(sk.size(), 99u);
        iter = sk.find(test{.a = 5, .b = 0});
        ASSERT_EQ(iter, sk.end());
        iter = sk.find(test{.a = 6, .b = 0});
        ASSERT_NE(iter, sk.end());
        std::stringstream ss;
        ss << iter;
        ASSERT_EQ(ss.str(), std::string("{a: 6, b: 7}"));
        ss = std::stringstream();
        ss << sk.end();
        ASSERT_EQ(ss.str(), std::string("[empty node]"));
    }
    {
        SubTestLog("skiplist other p and height");
        skiplist<int, std::less<int>, std::allocator<int>, 4> sk(1);
        sk.put(1);
        auto iter = sk.find(1);
        ASSERT_TRUE(iter != sk.end());
        ASSERT_NE(iter.node_, nullptr);
        ASSERT_EQ(iter.node_->height(), 4);
    }
    {
        SubTestLog("self alloc");
        auto alloc = Allocator::NewAlloc();
        int64_t base_size = sizeof(Allocator);
        ASSERT_EQ(alloc->Size(), base_size);
        ScopeGuard([&]() {
            ASSERT_EQ(alloc->Size(), base_size);
            Allocator::DeleteAlloc(alloc);
        });
        skiplist<int, std::less<int>, Allocator::StlWrapper<int>, 4> sk(1, alloc->GetStlWrapper<int>());
        typedef skiplist<int, std::less<int>, Allocator::StlWrapper<int>, 4>::NodeType NodeType;
        ASSERT_EQ(alloc->Size(), base_size + int64_t(sizeof(NodeType) + 4 * sizeof(NodeType*)));
        sk.put(123);
        ASSERT_EQ(alloc->Size(), base_size + 2 * int64_t(sizeof(NodeType) + 4 * sizeof(NodeType*)));
    }
}

} // namespace skiplist
} // namespace q_cpp_lib
